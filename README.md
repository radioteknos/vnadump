## vnadump - simple sw to dump *VNA measurements

It's quite simple to dump nanoVNA v1 once measurement has been done
using the instrument's user interfaces. Popular sw `nanovnasaver`
works fine though it's a bit bulky and is written in python causing
often the needs to reinstall it with all new library, packages and so
on.

With liteVNA `nanovnasaver` works in a very unusefull way: it cannot
grab calibrated data; it cannot get actually start, stop frequency and
other sweep params but this is a "problem" (choice?) of liteVNA and
nanoVNA v2 fw.

Thanks to suggetions from [LiteVNA] list and from DiSlord has been
possible to write simple sw (written in C) to dump calibrated data
from liteVNA and (soon) nanoVNA v1.

Next a similar sw to update liteVNA fw will be written (if dfu-util
won't work with liteVNA)

`vnadump` comes with some utilities to handle .s2p files:

1. s2pj allow to join two .s2p files filled with valid data only for
   S11 and S22 into one .s2p data with all four S-params. This is
   usefull aince alle \*VNA just measure S11 and S21 so one can swap
   the two measurement ports to get S12 and S22, next using `s2pj` a
   complete file can be obtained
2. s2ph2f: if you feel comfortable with a perfect simmetry of the two
   port device under measurement rhis util copy S11 to S22 and S21 to
   S12; at your own risk
3. s2p2s1p: since `vnadump` produces .s2p file (with last columns to
   zero) if DUT is one-port this util convert a .s2p file in a .s1p
   using only S11 or S22

All this utilities are tested under `qucs`, rune them with `-h` and
`-d` to get more infos.

`vnadump` has been developed under Debian gnu/linux, no plan to port
it on other OS.

{Project started on [2023-05-26]}

## installation
vnadump don't require particular library, just stdlib and math
lib. It's based on termios and  porting on other OS is not in
todo list.
gcc and autotools should be enough to compile. Follow the standard
procedure to compile:

	$ sh autogen.sh
	$ ./configure
	$ make
	$ sudo make install

If you want to install in some particular dir use

	$ ./configure prefix=/<somedir>
	
and installatio n will be done in <somedir>/bin/ isteead of /usr/local/bin/


## how to run

Just plug \*VNA on usb, ports should be /det/ttyACM0 but if you have
more than one \*VNA or other similar device use --port
<path_to_device_driver>.

Next run:

	$ vnadump --litevna --fstart <selected_start_freq> --fstop <selected_start_freq> --npt <number_of_point> > <filename>.s2p
	
where sweep parameter has to be copied from instrument display, no way
to get them from liteVNA and nanoVNA v2 protocol. Dumped data will be
placed in .s2p file.

It's usefull to runa `vnadump` with `-h` and `-d` to get more info anc
full explainations of cmd line params.

Next can be usefull to works on .s2p file so produced with `s2pj`,
`s2ph2f` and `s2p2s1p`; take a look at examples dir.

## feedback
Let me know,  write me, call me:

    Lapo Pieri
    via delle Ortensie, 22
	50142 FIRENZE

	<lapo@radioteknos.it>
	<ik5nax@radioteknos.it>
	<http://www.radioteknos.it/index_en.html>
	<http://www.radioteknos.it/ik5nax_en.html>
	T. +39 055 706881 - +39 329 2599801

