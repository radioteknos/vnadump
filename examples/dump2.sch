<Qucs Schematic 1.0.0>
<Properties>
  <View=42,101,804,865,1.13089,0,0>
  <Grid=10,10,1>
  <DataSet=dump2.dat>
  <DataDisplay=dump2.dpl>
  <OpenDisplay=0>
  <Script=dump2.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Pac P1 1 140 300 18 -26 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 140 330 0 0 0 0>
  <Pac P2 1 500 300 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 500 330 0 0 0 0>
  <.SP SP1 1 150 400 0 68 0 0 "log" 1 "2 GHz" 1 "3 GHz" 1 "301" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <GND * 1 330 260 0 0 0 0>
  <Eqn Eqn1 1 610 430 -28 15 0 0 "dBS21=dB(S[2,1])" 1 "dBS11=dB(S[1,1])" 1 "yes" 0>
  <SPfile X1 1 330 200 -26 -59 0 0 "/home/lapo/rtk/rft/vnadump/prove/dump2.s2p" 1 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
</Components>
<Wires>
  <330 230 330 260 "" 0 0 0 "">
  <140 200 300 200 "" 0 0 0 "">
  <140 200 140 270 "" 0 0 0 "">
  <360 200 500 200 "" 0 0 0 "">
  <500 200 500 270 "" 0 0 0 "">
</Wires>
<Diagrams>
  <Rect 160 750 345 171 3 #c0c0c0 1 00 1 0 0.2 1 1 -0.1 0.5 1.1 1 -0.1 0.5 1.1 315 0 225 0 0 0 "" "" "">
	<"dBS11" #0000ff 0 3 0 0 0>
	<"dBS21" #ff0000 0 3 0 0 0>
  </Rect>
  <Smith 570 750 187 187 3 #c0c0c0 1 00 1 0 1 1 1 0 4 1 1 0 1 1 315 0 225 0 0 0 "" "" "">
	<"S[1,1]" #0000ff 0 3 0 0 0>
  </Smith>
</Diagrams>
<Paintings>
</Paintings>
