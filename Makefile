package = vnadump
version = v1.0
tarname = vnadump

prefix      = /opt

distdir=$(tarname)-$(version)

export prefix package

all clean install uninstall $(package) s2pj s2ph2f:
	cd src && $(MAKE) $@
#	cd util && $(MAKE) $@
	-rm -rf *~


Makefile: Makefile.in config.status
	./config.status $@

config.status: configure
	./config.status --recheck

(distdir): FORCE
	mkdir -p $(distdir)/src $(distdir)/include $(distdir)/util
	cp configure.ac $(distdir)
	cp configure $(distdir)
	cp config.h.in $(distdir)
	cp install-sh $(distdir)
	cp Makefile.in $(distdir)
	cp src/Makefile.in $(distdir)/src
	cp src/*.c $(distdir)/src
#	cp util/Makefile.in  $(distdir)/util
	cp include/*.h $(distdir)/include
	cp configure.ac $(distdir)
	cp configure $(distdir)
	cp $(srcdir)/config.h.in $(distdir)
	cp $(srcdir)/install-sh $(distdir)


FORCE:
	-rm $(distdir).tar.gz >/dev/null 2>&1
	-rm -rf $(distdir) >/dev/null 2>&1

.PHONY: FORCE all clean dist install uninstall
