/*
 *   vnadump
 * 
 *      (c) by Lapo Pieri 2023
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#define LINEBUFFLEN 255
#define SELECTTO 100000  /* 100ms */

/* TODO: find a way to link version to git ver tag */
#define __VERSION "v1.2"

#define BUFFLEN 128

#define ANSI_RESET        "\033[0:00m"
#define ANSI_BOLD         "\033[0:01m"
#define ANSI_NORMAL       "\033[0:22m"
#define ANSI_ITALIC       "\033[0:03m"
#define ANSI_UNDERLINE    "\033[0:04m"
#define ANSI_BLINK        "\033[0:05m"
#define ANSI_REVERSE      "\033[0:07m"
#define ANSI_FG_BLACK     "\033[0:30m"
#define ANSI_FG_RED       "\033[0:31m"
#define ANSI_FG_GREEN     "\033[0:32m"
#define ANSI_FG_YELLOW    "\033[0:33m"
#define ANSI_FG_BLUE      "\033[0:34m"
#define ANSI_FG_MAGENTA   "\033[0:35m"
#define ANSI_FG_CYAN      "\033[0:36m"
#define ANSI_FG_WHITE     "\033[0:37m"
#define ANSI_BG_BLACK     "\033[0:40m"
#define ANSI_BG_RED       "\033[0:41m"
#define ANSI_BG_GREEN     "\033[0:42m"
#define ANSI_BG_YELLOW    "\033[0:43m"
#define ANSI_BG_BLUE      "\033[0:44m"
#define ANSI_BG_MAGENTA   "\033[0:45m"
#define ANSI_BG_CYAN      "\033[0:46m"
#define ANSI_BG_WHITE     "\033[0:47m"

/* *VNA type enumeration */
#define VNATYPE_UNSPEC    0x00
#define VNATYPE_LITEVNA   0x01
#define VNATYPE_NANOVNA   0x02

/* s1p s2p file format */
#define FMT_RI 1  /* real, imag */
#define FMT_MA 2  /* mag, angle */ /* angles in deg */
#define FMT_DB 3  /* db, angle */  /* angles in deg */
