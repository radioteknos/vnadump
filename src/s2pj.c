/*
 *   join two s2p files
 * 
 *      (c) by Lapo Pieri 2022-2023
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include "s2pj.h"


int main(int argc, char **argv){
  int clo;
  char linebuff1[LINEBUFFLEN+1], *lb1, linebuff2[LINEBUFFLEN+1], *lb2;
  double f1, f2, s1, s2, s3, s4, s5, s6, s7, s8;
  FILE *fp1, *fp2;
  
  /* = cmd line scan = */
  while((clo=getopt(argc, argv, "hda"))!=-1){
    switch(clo){
    case 'h':
      use();
      return 0;
      break;
    case 'd':
      doc();
      return 0;
    default:
      return 1;
    }
  }
  /* # cmd line scan end # */

  if((argc-optind)!=2){
    fprintf(stderr, "two .s2p files must be specified!\n");
    return 1;
  }

  /* = open source file = */
  if((fp1=fopen(argv[optind], "r"))==NULL) { 
    fprintf(stderr, "Unable to open first .s2p file %s\n", argv[optind]);
    return 1;
  }
  if((fp2=fopen(argv[optind+1], "r"))==NULL) { 
    fprintf(stderr, "Unable to open second .s2p file %s\n", argv[optind]);
    return 1;
  }
  /* # open source file end # */
  
  
  /* = .s2p files parsing = */
  while ((fgets(linebuff1, LINEBUFFLEN, fp1)!=NULL) &&
	 (fgets(linebuff2, LINEBUFFLEN, fp2)!=NULL)) {

    /* flush off spaces and tabs */
    lb1=(char*)linebuff1;
    skipspace(&lb1);
    stripnewline(&lb1);
    lb2=(char*)linebuff2;
    skipspace(&lb2);
    stripnewline(&lb2);

  /* discard comment lines (beginning with '#') */
    if(lb1[0]=='!')
      continue;
    if(lb1[0]=='#')
      printf("%s\n", lb1);
    else{
      sscanf(lb1, "%lf %lf %lf %lf %lf", &f1, &s1, &s2, &s3, &s4);
      sscanf(lb2, "%lf %lf %lf %lf %lf", &f2, &s5, &s6, &s7, &s8);
      if(f1!=f2){
	fprintf(stderr, "%s and %s not enough similar: freq don't match!\n",
		argv[optind], argv[optind+1]);
	return 1;
      }

      printf("%lf %lf  %lf  %lf  %lf  %lf  %lf  %lf  %lf\n",
	     f1, s1, s2, s3, s4, s7, s8, s5, s6);
      
    }
  }
  /* # .s2p files parsing end # */

  fclose(fp2);
  fclose(fp1);
  
  return 0;
}


void use(void){

  printf("\njoin two s2p files  ("
	 ANSI_FG_GREEN ANSI_BOLD "s2pj" ANSI_FG_WHITE ANSI_NORMAL
	 ") usage:\n\n");
  printf(
     "s2pj [options] <s11_s21.s2p> <s12_s22.s2p> > <full.s2p>\n");
  printf("-h this help\n");
  printf("-d show documentation and theory used \
(use $ s2ph2f -d |less)\n\n");
}

void doc(void){
  
  printf("\n  s2p half to full ("
	 ANSI_FG_GREEN ANSI_BOLD "s2pj" ANSI_FG_WHITE ANSI_NORMAL
	 ") join two half filled .s2p file the first with S11 and S21\n");
  printf("  and the second with S12 and S22 in a single full .s2p fileh\n");
  printf("  usefull for 2-ports VNAs\n\n");

}

unsigned int skipspace(char **b){
  while(((*b)[0]==' ' || (*b)[0]=='\t') && (*b)[0]!='\0') 
    (*b)++;
  if(*b[0]=='\0')
    return 1;
  else
    return 0;
}

unsigned int skipnotspace(char **b){
  while(*b[0]!=' ' && *b[0]!='\t' && *b[0]!='\0')
    (*b)++;
  if(*b[0]=='\0')
    return 1;
  else
    return 0;
}

unsigned int stripnewline(char **b){
  while((*b)[strlen(*b)-1]=='\r' || (*b)[strlen(*b)-1]=='\n')
    (*b)[strlen(*b)-1]='\0';
  
  return 0;
}
