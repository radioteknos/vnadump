/*
 *   vnadumo
 * 
 *      (c) by Lapo Pieri 2023
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>
#include <fcntl.h>
#include <ctype.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <math.h>
#include "misc.h"
#include "litevna.h"
#include "nanovna_v1.h"
#include "main.h"


int ml=1, verbose, fdser;

int main(int argc, char **argv){
  int clo, optidx=0;
  char serialportname[BUFFLEN]="/dev/ttyACM0";
  int serialspeed=38400;
  double fstart, fstop, fincr;
  int npts, i;
  int vnatype, snp_fmt=FMT_RI;
  double *s11_re, *s11_im, *s21_re, *s21_im;
  
  
  /* catch ctrl+C */
  signal(SIGINT, quit);

  /* = var init (pre-cmdline scan) = */
  verbose=0;
  fstart=50e3;
  fstop=6.3e9;
  npts=201;
  vnatype=VNATYPE_LITEVNA;
  /* # var init end # */
  
  /* = cmd line scan = */
  opterr=0;
  if(argc>1) {

    static struct option long_options[] = {
      {"port",      required_argument, 0,  0 },
      {"version",   no_argument,       0,  0 },
      {"doc",       no_argument,       0,  0 },
      {"help",      no_argument,       0,  0 },
      {"fstart",    required_argument, 0,  0 },
      {"fstop",     required_argument, 0,  0 },
      {"npts",      required_argument, 0,  0 },
      {"litevna",   no_argument,       0,  0 },
      {"nanovna",   no_argument,       0,  0 },
      {"nanovnaV1", no_argument,       0,  0 },
      {"ri",        no_argument,       0,  0 },
      {"ma",        no_argument,       0,  0 },
      {"db",        no_argument,       0,  0 },
      {0,           0,                 0,  0 }
    };
    
    while(1){
      clo=getopt_long(argc, argv, "hdv",
		      long_options, &optidx);
      if(clo==-1)
	break;

      switch(clo){
	/* = long options = */
      case 0:
	if(strcasecmp(long_options[optidx].name, "port")==0){
	  strncpy(serialportname, optarg, BUFFLEN-1);
	}
	/* TODO: unallowde for nanoVNA, check */
	else if(strcasecmp(long_options[optidx].name, "fstart")==0){
	  if(sscanf(optarg, "%lf", &fstart)!=1){
	    fprintf(stderr, "wrong value for --fstart cmd line option: %s",
		    optarg);
	    return 1;
	  }
	}
	/* TODO: unallowde for nanoVNA, check */
	else if(strcasecmp(long_options[optidx].name, "fstop")==0){
	  if(sscanf(optarg, "%lf", &fstop)!=1){
	    fprintf(stderr, "wrong value for --fstop cmd line option: %s",
		    optarg);
	    return 1;
	  }
	}
	/* TODO: unallowde for nanoVNA, check */
	else if(strcasecmp(long_options[optidx].name, "npts")==0){
	  if(sscanf(optarg, "%d", &npts)!=1){
	    fprintf(stderr, "wrong value for --npts cmd line option: %s",
		    optarg);
	    return 1;
	  }
	}
	else if(strcasecmp(long_options[optidx].name, "litevna")==0){
	  vnatype=VNATYPE_LITEVNA;
	}
	else if( (strcasecmp(long_options[optidx].name, "nanovna")==0) ||
	  (strcasecmp(long_options[optidx].name, "nanovnaV1")==0)) {
	  vnatype=VNATYPE_NANOVNA;
	}
	else if(strcasecmp(long_options[optidx].name, "ri")==0){
	  snp_fmt=FMT_RI;
	}
	else if(strcasecmp(long_options[optidx].name, "ma")==0){
	  snp_fmt=FMT_MA;
	}
	else if(strcasecmp(long_options[optidx].name, "db")==0){
	  snp_fmt=FMT_DB;
	}
	else if(strcasecmp(long_options[optidx].name, "version")==0){
	  printf("%s\n", __VERSION);
	  return 0;
	}
	else if(strcasecmp(long_options[optidx].name, "help")==0){
	  use();
	  return 0;
	}
	else if(strcasecmp(long_options[optidx].name, "doc")==0){
	  doc();
	  return 0;
	}
	else{                                      /* never should get here */
	  fprintf(stderr, "option %s", long_options[optidx].name);
	  if(optarg)
	    fprintf(stderr, " with arg %s", optarg);
	  printf("\n");
	}
	break;
      /* # long options end # */
	
      case 'h':
	use();
	return 0;
      case 'd':
	doc();
	return 0;
	break;	
      case 'v':
	verbose=1;
	break;
      case '?':                                            /* wrong options */
	fprintf(stderr, "wrong arg: %s (%s)\n", argv[optind-1],
		argv[optind]);	
	return 1;
      default:
	fprintf(stderr, "internal error (getopt_long(): %c\n", clo);
	
      }
    }
  }
  /* # cmd line scan end # */

  /* = alloc data mem = */
  s11_re=(double *)malloc(sizeof(double)*npts);
  s11_im=(double *)malloc(sizeof(double)*npts);
  s21_re=(double *)malloc(sizeof(double)*npts);
  s21_im=(double *)malloc(sizeof(double)*npts);
  /* # alloc data mem end # */

  /* = variable init (post-cmdline scan) = */
  fincr=(fstop-fstart)/(npts-1);
  /* # variable init (post-cmdline scan) end # */
  
  /* = open serial port = */
  if((fdser=serial_open(serialportname, serialspeed))==-1){
    fprintf(stderr, "Unable to open serial port %s\n",
	    serialportname);
    return(1);
  }
  /* # open serial port end # */  

  /* = liteVNA = */
  if(vnatype==VNATYPE_LITEVNA){
    /* flush */
    if(litevna_flush()!=0){
      fprintf(stderr, "Error flushing liteVNA i/f\a\n");
      return 1;
    }
    
    /* detect */
    if(litevna_detect()!=0){
      fprintf(stderr, "liteVNA not detected\n");
      return 1;
    }
    
    /* send 0x20 0x26 0x03 to set calibrated data mode */
    if(litevna_cal_data_set()!=0){
      fprintf(stderr, "Error while setting calibrated data output\a\n");
      return 1;
    }
    
    /* dump fifo */
    if(litevna_fifo_dump(npts, s11_re, s11_im, s21_re, s21_im)!=0){
      fprintf(stderr, "Error while getting fifo data\a\n");
      return 1;
    }

    /* print .s2p file */
    printf("! vnadump generated file\n");
    printf("! https://gitlab.com/radioteknos/vnadump.git\n");
    printf("! data from LiteVNA\n"); 
    printf("! $ ");
    for(i=0; i<argc; i++){
      printf("%s ", argv[i]);
    }
    printf("\n!\n");
    switch(snp_fmt){
    case FMT_RI:
      printf("# Hz S RI R 50.0\n");
      printf("!freq S11_re S11_im S21_re S21_im S12_re S12_im S22_re S22_im\n"
	     );    
      for(i=0; i<npts; i++){
	printf("%lf\t", fstart+fincr*i);
	printf("%13.9lf\t%13.9lf\t%13.9lf\t%13.9lf\t0.0 0.0 0.0 0.0\n",
	       s11_re[i], s11_im[i], s21_re[i], s21_im[i]);
      }
      break;
    case FMT_MA:
      printf("# Hz S MA R 50.0\n");
      printf("!freq S11_mag S11_phs S21_mag S21_phs S12_mag S12_phs \
S22_mag S22_phs\n");    
      for(i=0; i<npts; i++){
	printf("%lf\t", fstart+fincr*i);
	printf("%13.9lf\t%13.9lf\t%13.9lf\t%13.9lf\t0.0 0.0 0.0 0.0\n",
	       sqrt(s11_re[i]*s11_re[i]+s11_im[i]*s11_im[i]),
	       atan2(s11_im[i], s11_re[i])*180/M_PI,
	       sqrt(s21_re[i]*s21_re[i]+s21_im[i]*s21_im[i]),
	       atan2(s21_im[i], s21_re[i])*180/M_PI);
      }
      break;
    case FMT_DB:
      printf("# Hz S DB R 50.0\n");
      printf("!freq S11_db S11_phs S21_db S21_phs S12_db S12_phs \
S22_db S22_phs\n");    
      for(i=0; i<npts; i++){
	printf("%lf\t", fstart+fincr*i);
	printf("%13.9lf\t%13.9lf\t%13.9lf\t%13.9lf\t0.0 0.0 0.0 0.0\n",
	       10*log10(s11_re[i]*s11_re[i]+s11_im[i]*s11_im[i]),
	       atan2(s11_im[i], s11_re[i])*180/M_PI,
	       10*log10(s21_re[i]*s21_re[i]+s21_im[i]*s21_im[i]),
	       atan2(s21_im[i], s21_re[i])*180/M_PI);
      }
      break;
    default:
      fprintf(stderr, "Internal error: invalid format: %d\n", snp_fmt);
      return 1;
    }
  }
  /* # liteVNA end # */

  /* = nanoVNA v1 = */
  else if(vnatype==VNATYPE_NANOVNA){
    /* flush */
    if(nanovna_flush()!=0){
      fprintf(stderr, "Error flushing nanoVNA i/f\a\n");
      return 1;
    }
    
    /* detect */
    if(nanovna_detect()!=0){
      fprintf(stderr, "nanoVNA not detected\n");
      return 1;
    }

    /* dump freqs */
    if(nanovna_sweep_params_get(&npts, &fstart, &fstop)!=0){
      fprintf(stderr, "Error while getting freqs\a\n");
      return 1;
    }
    fincr=(fstop-fstart)/(npts-1);

    /* dump data */
    if(nanovna_data_dump(npts, s11_re, s11_im, s21_re, s21_im)!=0){
      fprintf(stderr, "Error while getting data\a\n");
      return 1;
    }

    /* print .s2p file */
    printf("! vnadump generated file\n");
    printf("! https://gitlab.com/radioteknos/vnadump.git\n");
    printf("! data from nanoVNA\n"); 
    printf("! $ ");
    for(i=0; i<argc; i++){
      printf("%s ", argv[i]);
    }
    printf("\n!\n");
    switch(snp_fmt){
    case FMT_RI:
      printf("# Hz S RI R 50.0\n");
      printf("!freq S11_re S11_im S21_re S21_im S12_re S12_im S22_re S22_im\n"
	     );    
      for(i=0; i<npts; i++){
	printf("%lf\t", fstart+fincr*i);
	printf("%13.9lf\t%13.9lf\t%13.9lf\t%13.9lf\t0.0 0.0 0.0 0.0\n",
	       s11_re[i], s11_im[i], s21_re[i], s21_im[i]);
      }
      break;
    case FMT_MA:
      printf("# Hz S MA R 50.0\n");
      printf("!freq S11_mag S11_phs S21_mag S21_phs S12_mag S12_phs \
S22_mag S22_phs\n");    
      for(i=0; i<npts; i++){
	printf("%lf\t", fstart+fincr*i);
	printf("%13.9lf\t%13.9lf\t%13.9lf\t%13.9lf\t0.0 0.0 0.0 0.0\n",
	       sqrt(s11_re[i]*s11_re[i]+s11_im[i]*s11_im[i]),
	       atan2(s11_im[i], s11_re[i])*180/M_PI,
	       sqrt(s21_re[i]*s21_re[i]+s21_im[i]*s21_im[i]),
	       atan2(s21_im[i], s21_re[i])*180/M_PI);
      }
      break;
    case FMT_DB:
      printf("# Hz S DB R 50.0\n");
      printf("!freq S11_db S11_phs S21_db S21_phs S12_db S12_phs \
S22_db S22_phs\n");    
      for(i=0; i<npts; i++){
	printf("%lf\t", fstart+fincr*i);
	printf("%13.9lf\t%13.9lf\t%13.9lf\t%13.9lf\t0.0 0.0 0.0 0.0\n",
	       10*log10(s11_re[i]*s11_re[i]+s11_im[i]*s11_im[i]),
	       atan2(s11_im[i], s11_re[i])*180/M_PI,
	       10*log10(s21_re[i]*s21_re[i]+s21_im[i]*s21_im[i]),
	       atan2(s21_im[i], s21_re[i])*180/M_PI);
      }
      break;
    default:
      fprintf(stderr, "Internal error: invalid format: %d\n", snp_fmt);
      return 1;
    }
    
  }
  /* # nanoVNA v1 end # */

  free(s11_re);
  free(s11_im);
  free(s21_re);
  free(s21_im);
  
  close(fdser);
  return 0;
}
