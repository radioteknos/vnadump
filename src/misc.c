/*
 *   vnadump
 * 
 *      (c) by Lapo Pieri 2023
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <sys/ioctl.h>
#include <signal.h>
#include <termios.h>
#include <fcntl.h>
#include "main.h"
#include "misc.h"

extern int ml, verbose, fdser;

struct b2tt{
  int baudrate;
  int termiosrate;
} baud2termiostable[]={
		       /* Posix */
		       {0,      B0}, /* hang up */
		       {50,     B50},
		       {75,     B75},
		       {110,    B110},
		       {134,    B134},
		       {150,    B150},
		       {200,    B200},
		       {300,    B300},
		       {600,    B600},
		       {1200,   B1200},
		       {1800,   B1800},
		       {2400,   B2400},
		       {4800,   B4800},
		       {9600,   B9600},
		       {19200,  B19200},
		       {38400,  B38400},
		       /* not-Posix*/ 
		       {57600,    B57600},
		       {115200,   B115200},
		       {230400,   B230400},
		       {460800,   B460800},
		       {500000,   B500000},
		       {576000,   B576000},
		       {921600,   B921600}, 
		       {1000000,  B1000000},
		       {1152000,  B1152000},
		       {1500000,  B1500000},
		       {2000000,  B2000000},
		       {2500000,  B2500000},
		       {3000000,  B3000000},
		       {3500000,  B3500000},
		       {4000000,  B4000000}
		       /* #define __MAX_BAUD B4000000 */
};

unsigned int skipspace(char **b){
  while(((*b)[0]==' ' || (*b)[0]=='\t') && (*b)[0]!='\0') 
    (*b)++;
  if(*b[0]=='\0')
    return 1;
  else
    return 0;
}

unsigned int skipnotspace(char **b){
  while(*b[0]!=' ' && *b[0]!='\t' && *b[0]!='\0')
    (*b)++;
  if(*b[0]=='\0')
    return 1;
  else
    return 0;
}

unsigned int stripnewline(char **b){
  while((*b)[strlen(*b)-1]=='\r' || (*b)[strlen(*b)-1]=='\n')
    (*b)[strlen(*b)-1]='\0';
  
  return 0;
}

unsigned int firsttoken(char *b, char *t){
  int i;

  i=0;
  while(b[i]!='\0' && b[i]!=' '){
    t[i]=b[i];
    i++;
  }
  t[i]='\0';
  
  return 0;
}

int baud2termios(int baudrate){
  unsigned int i;
  
  for(i=0; i<(int)sizeof(baud2termiostable)/
	sizeof(baud2termiostable[0]); i++){
    if(baud2termiostable[i].baudrate==baudrate){
      return baud2termiostable[i].termiosrate;
    }
  }

  return -1; /* invalid baud rate */
}

int serial_open(char *sfn, int speed){
int fd;
struct termios sersettings;

 if((fd=open(sfn, O_RDWR|O_NOCTTY/* |O_NDELAY */))!=-1) {
   bzero(&sersettings, sizeof(sersettings));

   
   sersettings.c_cflag=baud2termios(speed)|CS8|CLOCAL|CREAD;
   sersettings.c_iflag=IGNPAR | /* IXON | IXOFF | */ IGNBRK;
   sersettings.c_oflag=0;
   /* sersettings.c_lflag=ICANON; */ /* raw mode */
   sersettings.c_cc[VTIME]=0;
   sersettings.c_cc[VMIN]=0;
   tcflush(fd, TCIOFLUSH);
   tcsetattr(fd, TCSANOW, &sersettings);
   tcflush(fd, TCIOFLUSH);                    /* flush rx and tx buffer */
 }

 return fd;
}

int serial_send_ascii(char *b){
  int i;

  if(verbose>0)
    fprintf(stderr, "serial send: ");
  for(i=0; i<(int)strlen(b); i++) {
    write(fdser, b+i, 1);
    if(verbose>0)
      fprintf(stderr, "%02x ", b[i]);
    usleep(2000); /* TODO: adj/remove? */
  }
  if(verbose>0)
    fprintf(stderr, "\n");
  
  return(0);
}

int serial_send_bin(char *b, int len){
  int i;

  if(verbose>0)
    fprintf(stderr, "serial send: ");
  for(i=0; i<len; i++) {
    write(fdser, b+i, 1);
    if(verbose>0)
      fprintf(stderr, "%02x ", b[i]);
    usleep(2000); /* TODO: remove */
  }
  if(verbose>0)
    fprintf(stderr, "\n");
  
  return(0);
}

void use(void){

  printf("\n            *VNA dump script %s ("
	 ANSI_FG_GREEN ANSI_BOLD "vnadump" ANSI_FG_WHITE ANSI_NORMAL
	 ") \n", __VERSION);
  printf("      (c) by Lapo Pieri 2023 under gnu/gpl3 license\n");
  printf("\nusage:\n\n");
  printf("  vnadump [options] \n");
  printf("    -h --help this help\n");
  printf("    -v verbose printout (for debug purpose: do not use!)\n");
  printf("    -d --doc show documentation (use $ vnadump -d |less)\n");
  printf("    --version print current version\n\n");

  printf("    --litevna select liteVNA protocol (default)\n");
  printf("    --nanovna --nanoVNAV1 select nanoVNA V1 protocol \n\n");

  printf("    --port <port> select serial port to *VNA\
 (default /dev/ttyACM0)\n");
  printf("    --fstart <fstart> set start freq [Hz], no prefix\
 (default 50kHz)\n");
  printf("    --fstop <fstop> set stop freq [Hz], no prefix\
 (default 6.3GHz)\n");
  printf("    --npts <npts> set number of points (default 201)\n\n");
  printf("    --ri real-imag output in .s2p file (default)\n");
  printf("    --ma mag-angle output in .s2p file\n");
  printf("    --ri db-angle output in .s2p file\n\n");
  
  printf("use ctrl+C to interrupt operations\n\n");
}

void doc(void){
  
  printf("\n *VNA dump script %s ("
	 ANSI_FG_GREEN ANSI_BOLD "vnadump" ANSI_FG_WHITE ANSI_NORMAL
	 ") is a simple script that allow\n\
 data dump from *vna (nanoVna, LiteVNA, etc...)\n",
	 __VERSION);
  printf("      (c) by Lapo Pieri 2023 under gnu/gpl3 license\n\n");

  printf("\
 Communication protocol with LiteVNA has been gathered from\n\
  liteVNA user manual (LiteVNA_User-Guide.pdf) based on fw v1.3 and\n\
  with some hints by DiSlord and other users on [liteVNA] list on\n\
  liteVNA@groups.io\n\
 In particular sequence 0x20 0x26 0x03 has been suggested to get\n\
  calibrated data.\n\
 There's a great lack on information but protocol is simple and vnadump\n\
  seems works fine\n\
 Some ideas has been gathered from nanovnasaver source code\n\
 Test has been performed on liteVNA hw v64-0.3.1 and fw v1.3.07\n\
 Touchstone file format (.s2p) is v1.1 found on touchstone_spec11.pdf\n\
 Communication protocol with nanoVNA has been gathered from\n\
  help command build in nanoVNA itself and some tests on nanoVNA\n\
  version 0.7.1-1-g1656342\n\
 nanoVNA allow retrivial of f_start, f_stop and number of points (always\n\
 101), do not use cmd line --fstart, --fstop and --hpts options\n\
\n");
    
}

void quit(int signo){
  if(signo==SIGINT){
    ml=0;
  }

  printf("\nuser exit request (Ctrl+C)\n");
  
}
