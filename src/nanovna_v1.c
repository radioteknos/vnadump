/*
 *   vnadumo
 * 
 *      (c) by Lapo Pieri 2023
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>
#include <fcntl.h>
#include <ctype.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <math.h>
#include "misc.h"
#include "main.h"
#include "nanovna_v1.h"

extern int ml, verbose, fdser;
extern double s11_re[], s11_im[], s21_re[], s21_im[];

/* put liteVNA protocol (and other binary protocols) in defined state */
int nanovna_flush(void){
  char msg[BUFFLEN];
  int rv, n, i, lastfd=0;
  fd_set rfds;
  struct timeval timeout;

  if(lastfd<fdser)
    lastfd=fdser;
  FD_ZERO(&rfds);
  FD_SET(fdser, &rfds);
  lastfd++;
  timeout.tv_sec=0;
  timeout.tv_usec=20000;

  while(1){
    rv=select(lastfd, &rfds, NULL, NULL, &timeout);

    if(ml==0){
      close(fdser);
      exit(1);
    }
    
    switch(rv){
    case -1: /* select error */
      perror("select(): ");
      return -1;
    case 0:
      return 0; /* flushing complete no (more) residual data available */
    default:
      n=read(fdser, msg, BUFFLEN); /* some spurious data received */
      if(verbose==1){
	fprintf(stderr, "spurious data: ");
	for(i=0; i<n; i++){
	  if(i%20==0)
	    fprintf(stderr, "\n");
	  fprintf(stderr, "%02x  ", msg[i]);
	}
	fprintf(stderr, "\n");
      }
    }
    /* maybe other residual data could arise, loop*/
  }
  
}


/* nanoVNA detect */
int nanovna_detect(void){
  char msg[BUFFLEN];
  int rv, n, i, lastfd=0;
  fd_set rfds;
  struct timeval timeout;

  serial_send_ascii("\r");

  if(lastfd<fdser)
    lastfd=fdser;
  FD_ZERO(&rfds);
  FD_SET(fdser, &rfds);
  lastfd++;
  timeout.tv_sec=0;
  timeout.tv_usec=20000;

  rv=select(lastfd, &rfds, NULL, NULL, &timeout);

  if(ml==0){
    close(fdser);
    exit(1);
  }

  n=read(fdser, msg, BUFFLEN-1);
  msg[n]='\0';
  
  switch(rv){
  case -1:
    perror("select(): ");
    return -1; /* select error */
  case 0:
    return 1; /* timeout: nanoVNA don't answer */
  default:
    if(verbose==1){
      fprintf(stderr, "received data after 0x0d cmd: ");
      for(i=0; i<n; i++){
	if(i%20==0)
	  fprintf(stderr, "\n");
	fprintf(stderr, "%02x  ", msg[i]);
      }
      fprintf(stderr, "\n(%s)\n", msg);      
    }
    if(strcmp(msg, "\r\nch> ")==0)
      return 0;
    else{
      fprintf(stderr, "%s\n", msg);
      if(verbose==1){
	fprintf(stderr,
		"wrong data in reply to a <CR> (0x0d)\n");
      }
      return 1;
    }
  }
}

/* nanoVNA sweep params get */
int nanovna_sweep_params_get(int *npts, double *fmin, double *fmax){
  char msg[BUFFLEN], *lb;
  int rv, n, i, lastfd=0;
  fd_set rfds;
  struct timeval timeout;

  serial_send_ascii("sweep\r");

  if(lastfd<fdser)
    lastfd=fdser;
  FD_ZERO(&rfds);
  FD_SET(fdser, &rfds);
  lastfd++;
  timeout.tv_sec=0;
  timeout.tv_usec=20000;

  rv=select(lastfd, &rfds, NULL, NULL, &timeout);

  if(ml==0){
    close(fdser);
    exit(1);
  }

  n=read(fdser, msg, BUFFLEN-1);
  msg[n]='\0';
  
  switch(rv){
  case -1:
    perror("select(): ");
    return -1; /* select error */
  case 0:
    return 1; /* timeout: nanoVNA don't answer */
  default:
    if(verbose==1){
      fprintf(stderr, "received data after 0x0d cmd: ");
      for(i=0; i<n; i++){
	if(i%20==0)
	  fprintf(stderr, "\n");
	fprintf(stderr, "%02x  ", msg[i]);
      }
      fprintf(stderr, "\n(%s)\n", msg);
    }

    lb=strstr(msg, "sweep")+strlen("sweep");
    if((rv=sscanf(lb, "%lf %lf %d", fmin, fmax, npts))!=3){
      fprintf(stderr,
	      "wrong answer to sweep cmd, only %d params given\n(%s)\n",
	      rv, lb);
      return 1;      
    }
    if(verbose==1){
      fprintf(stderr,"%d %lf %lf\n", *npts, *fmin, *fmax);
    }
  }

  return 0;
}


/* nanoVNA data dump */
int nanovna_data_dump(int npts, double *s11_re, double *s11_im,
		      double *s21_re, double *s21_im){
  int n, lastfd=0, i;
  fd_set rfds;
  struct timeval timeout;
  char databuff[DATABUFFLEN], databuff2[DATABUFFLEN], *lb;
  double re, im;
  
  serial_send_ascii("data 0\r");

  n=1;
  databuff2[0]='\0';
  
  while(n!=0){
    if(lastfd<fdser)
      lastfd=fdser;
    FD_ZERO(&rfds);
    FD_SET(fdser, &rfds);
    lastfd++;
    timeout.tv_sec=0;
    timeout.tv_usec=1000000;
    
    select(lastfd, &rfds, NULL, NULL, &timeout);
    
    if(ml==0){
      close(fdser);
      exit(1);
    }
    
    n=read(fdser, databuff, DATABUFFLEN-1);
    databuff[n]='\0';
    strcat(databuff2, databuff);
    
  }

  lb=strstr(databuff2, "\r\n")+2;

  for(i=0; i<npts; i++){
    sscanf(lb, "%lf %lf", &re, &im);
    s11_re[i]=re;
    s11_im[i]=im;
    
    if(verbose==1){
      fprintf(stderr, "%13.9lf  %13.9lf\n", re, im);
    }
    lb=strstr(lb, "\r\n")+2;
  }
  
  serial_send_ascii("data 1\r");

  n=1;
  databuff2[0]='\0';
  
  while(n!=0){
    if(lastfd<fdser)
      lastfd=fdser;
    FD_ZERO(&rfds);
    FD_SET(fdser, &rfds);
    lastfd++;
    timeout.tv_sec=0;
    timeout.tv_usec=1000000;
    
    select(lastfd, &rfds, NULL, NULL, &timeout);
    
    if(ml==0){
      close(fdser);
      exit(1);
    }
    
    n=read(fdser, databuff, DATABUFFLEN-1);
    databuff[n]='\0';
    strcat(databuff2, databuff);
    
  }

  lb=strstr(databuff2, "\r\n")+2;

  for(i=0; i<npts; i++){
    sscanf(lb, "%lf %lf", &re, &im);
    s21_re[i]=re;
    s21_im[i]=im;
    
    if(verbose==1){
      fprintf(stderr, "%13.9lf  %13.9lf\n", re, im);
    }
    lb=strstr(lb, "\r\n")+2;
  }
    
  return 0;
}
