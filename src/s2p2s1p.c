/*
 *   s2p to s1p 
 * 
 *      (c) by Lapo Pieri 2023
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <getopt.h>
#include <math.h>
#include "s2p2s1p.h"


int main(int argc, char **argv){
  int ml=1, clo, optidx=0, single_port=1;
  char linebuff[LINEBUFFLEN+1], *lb;
  double f, s1, s2, s3, s4, s5, s6, s7, s8;
  
  /* = cmd line scan = */
  opterr=0;
  if(argc>1) {

    static struct option long_options[] = {
      {"help",      no_argument,       0,  0 },
      {"doc",      no_argument,       0,  0 },
      {"s11",      no_argument,       0,  0 },
      {"s22",      no_argument,       0,  0 },
      {0,           0,                 0,  0 }
    };
    
    while(1){
      clo=getopt_long(argc, argv, "hd",
		      long_options, &optidx);
      if(clo==-1)
	break;

      switch(clo){
	/* = long options = */
      case 0:
	if(strcasecmp(long_options[optidx].name, "s11")==0){
	  single_port=1;
	}
	else if(strcasecmp(long_options[optidx].name, "s22")==0){
	  single_port=2;
	}
	else if(strcasecmp(long_options[optidx].name, "version")==0){
	  printf("%s\n", __VERSION);
	  return 0;
	}
	else if(strcasecmp(long_options[optidx].name, "help")==0){
	  use();
	  return 0;
	}
	else if(strcasecmp(long_options[optidx].name, "doc")==0){
	  doc();
	  return 0;
	}
	else{                                      /* never should get here */
	  fprintf(stderr, "option %s", long_options[optidx].name);
	  if(optarg)
	    fprintf(stderr, " with arg %s", optarg);
	  printf("\n");
	}
	break;
      /* # long options end # */
	
      case 'h':
	use();
	return 0;
      case 'd':
	doc();
	return 0;
	break;	
      case '?':                                            /* wrong options */
	fprintf(stderr, "wrong arg: %s (%s)\n", argv[optind-1],
		argv[optind]);	
	return 1;
      default:
	fprintf(stderr, "internal error (getopt_long(): %c\n", clo);
	
      }
    }
  }
  /* while((clo=getopt(argc, argv, "hd"))!=-1){ */
  /*   switch(clo){ */
  /*   case 'h': */
  /*     use(); */
  /*     return 0; */
  /*     break; */
  /*   case 'd': */
  /*     doc(); */
  /*     return 0; */
  /*   default: */
  /*     return 1; */
  /*   } */
  /* } */
  /* # cmd line scan end # */
  
  /* = .s2p file parsing = */
  while(ml){
    /* read .n2s file from stdin */
    if(fgets(linebuff, LINEBUFFLEN, stdin)==NULL){
      ml=0;
      continue;
    }
    
    /* flush off spaces and tabs */
    lb=(char*)linebuff; skipspace(&lb);
    stripnewline(&lb);

    /* discard comment lines (beginning with '#') */
    if(lb[0]=='!')
      continue;
    if(lb[0]=='#')
      printf("%s\n", lb);
    else{
      scanf("%lf %lf %lf %lf %lf %lf %lf %lf %lf",
	    &f, &s1, &s2, &s3, &s4, &s5, &s6, &s7, &s8);
      if(single_port==1){
	printf("%lf %lf  %lf\n", f, s1, s2);
      }
      else if(single_port==2){
	printf("%lf %lf  %lf\n", f, s7, s8);
      }
    }
  }
  /* # .n2s file parsing end # */

  return 0;
}


void use(void){

  printf("\ns2p to s1p converter ("
	 ANSI_FG_GREEN ANSI_BOLD "s2p2s1p" ANSI_FG_WHITE ANSI_NORMAL
	 ") usage:\n\n");
  printf(
     "s2p2s1p [options] <.s2p> > <.s1pl>\n");
  printf("-h this help\n");
  printf("-d show documentation and theory used \
(use $ s2p2s1p -d |less)\n\n");
}

void doc(void){
  
  printf("\n  s2p to s1p converter ("
	 ANSI_FG_GREEN ANSI_BOLD "s2p2s1p" ANSI_FG_WHITE ANSI_NORMAL
	 ") is a simple batch that extract S11 from a .s2p file\n\n");
  
}

unsigned int skipspace(char **b){
  while(((*b)[0]==' ' || (*b)[0]=='\t') && (*b)[0]!='\0') 
    (*b)++;
  if(*b[0]=='\0')
    return 1;
  else
    return 0;
}

unsigned int skipnotspace(char **b){
  while(*b[0]!=' ' && *b[0]!='\t' && *b[0]!='\0')
    (*b)++;
  if(*b[0]=='\0')
    return 1;
  else
    return 0;
}

unsigned int stripnewline(char **b){
  while((*b)[strlen(*b)-1]=='\r' || (*b)[strlen(*b)-1]=='\n')
    (*b)[strlen(*b)-1]='\0';
  
  return 0;
}
