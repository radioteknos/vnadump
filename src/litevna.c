/*
 *   vnadumo
 * 
 *      (c) by Lapo Pieri 2023
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>
#include <fcntl.h>
#include <ctype.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <math.h>
#include "main.h"
#include "litevna.h"

extern int ml, verbose, fdser;
extern double s11_re[], s11_im[], s21_re[], s21_im[];

/* put liteVNA protocol (and other binary protocols) in defined state */
int litevna_flush(void){
  char msg[BUFFLEN];
  int rv, n, i, lastfd=0;
  fd_set rfds;
  struct timeval timeout;
  
  for(i=0; i<8; i++){
    msg[i]='\0';
  }

  write(fdser, msg, 8);

  if(lastfd<fdser)
    lastfd=fdser;
  FD_ZERO(&rfds);
  FD_SET(fdser, &rfds);
  lastfd++;
  timeout.tv_sec=0;
  timeout.tv_usec=20000;

  rv=select(lastfd, &rfds, NULL, NULL, &timeout);

  if(ml==0){
    close(fdser);
    exit(1);
  }
  
  switch(rv){
  case -1: /* select error */
    perror("select(): ");
    return -1;
  case 0:
    return 0; /* it's ok not receiving any answer so cmd is succesfull */
  default:
    n=read(fdser, msg, BUFFLEN); /* some spurious data received */
    if(verbose==1){
      fprintf(stderr, "spurious data received while sending nosp: ");
      for(i=0; i<n; i++){
	fprintf(stderr, "%02x  ", msg[i]);
	if(i%20==0)
	  fprintf(stderr, "\n");
      }
      fprintf(stderr, "\n");
    }
    return 1;
  }
  
}


/* liteVNA detect */
int litevna_detect(void){
  char msg[BUFFLEN];
  int rv, n, i, lastfd=0;
  fd_set rfds;
  struct timeval timeout;

  msg[0]=0x0d;

  write(fdser, msg, 1);


  if(lastfd<fdser)
    lastfd=fdser;
  FD_ZERO(&rfds);
  FD_SET(fdser, &rfds);
  lastfd++;
  timeout.tv_sec=0;
  timeout.tv_usec=20000;

  rv=select(lastfd, &rfds, NULL, NULL, &timeout);

  if(ml==0){
    close(fdser);
    exit(1);
  }

  n=read(fdser, msg, 32);

  switch(rv){
  case -1:
    perror("select(): ");
    return -1; /* select error */
  case 0:
    return 1; /* timeout: liteVNA don't answer */
  default:
    if(verbose==1){
      fprintf(stderr, "received data after 0x0d cmd: ");
      for(i=0; i<n; i++){
	fprintf(stderr, "%02x  ", msg[i]);
	if(i%20==0)
	  fprintf(stderr, "\n");
      }
      fprintf(stderr, "\n");
    }
    if(n==1 && msg[0]=='2'){
      return 0;
    }
    else{
      if(verbose==1){
	fprintf(stderr,
		"wrong data in reply of 0x0d cmd (should be '2' = 0x32)\n");
      }    
      return 1;
    }
  }
}


/* select calibrated data output */
int litevna_cal_data_set(void){
  char msg[BUFFLEN];
  int rv, n, i, lastfd=0;
  fd_set rfds;
  struct timeval timeout;
  
  msg[0]=0x20; msg[1]=0x26; msg[2]=0x03;
  
  write(fdser, msg, 3);

  if(lastfd<fdser)
    lastfd=fdser;
  FD_ZERO(&rfds);
  FD_SET(fdser, &rfds);
  lastfd++;
  timeout.tv_sec=0;
  timeout.tv_usec=20000;

  rv=select(lastfd, &rfds, NULL, NULL, &timeout);

  if(ml==0){
    close(fdser);
    exit(1);
  }
  
  switch(rv){
  case -1:
    perror("select(): ");
    return -1; /* select error */
  case 0:
    return 0; /* it's ok not receiving any answer so cmd is succesfull */
  default:
    n=read(fdser, msg, BUFFLEN); /* some spurious data received */
    if(verbose==1){
      fprintf(stderr,
	      "spurious data received while sending 0x20, 0x26, 0x03: ");
      for(i=0; i<n; i++){
	fprintf(stderr, "%02x  ", msg[i]);
	if(i%20==0)
	  fprintf(stderr, "\n");
      }
      fprintf(stderr, "\n");
    }
    return 1;
  }
  
}

/* select uncalibrated data output */
/* TODO */

/* liteVNA fifo dump */
int litevna_fifo_dump(int npts, double *s11_re, double *s11_im,
		      double *s21_re, double *s21_im){
  char msg[BUFFLEN];
  int rv, n, i, lastfd=0, j, byte_cnt, respts, bl, k;
  uint8_t npd;
  fd_set rfds;
  struct timeval timeout;
  uint8_t spt[32];
  int32_t wave[6];
  double fwd_pwr;
  uint16_t freq_idx;

  /* variable init */
  respts=npts;

  /* dumo in max 255 points at time */
  while(respts>0){

    /* set max length to be dunmped in each pass */
    if(respts>255){
      respts-=255;
      npd=255;
    }
    else{
      npd=respts;
      respts=0;
    }

    byte_cnt=0;
    
    /* send request */
    msg[0]=0x18; msg[1]=0x30; msg[2]=npd;
    write(fdser, msg, 3);
  
    /* select() init */
    if(lastfd<fdser)
      lastfd=fdser;
    FD_ZERO(&rfds);
    FD_SET(fdser, &rfds);
    lastfd++;
    timeout.tv_sec=0;
    timeout.tv_usec=250000;

    bl=0;  
    /* receive points */  
    for(j=0; j<npd; j++){

      /* = get single frequency point = */
      rv=select(lastfd, &rfds, NULL, NULL, &timeout);

      if(ml==0){
	close(fdser);
	exit(1);
      }

      switch(rv){
      case -1:
	perror("select(): ");
	return -1; /* select error */
      case 0:
	if(byte_cnt/32!=npd){
	  if(verbose==1){
	    fprintf(stderr, "\nselect() returns 0 [%d, %d]\n", byte_cnt, npd);
	  }
	  return 1; /* timeout: liteVNA don't answer */
	}
	else{
	  bl=1; /* break the loop if no more data */
	}
	break;
      default:
	n=read(fdser, msg, 32);
	byte_cnt+=n;
	if(n!=32){
	  if(verbose==1){
	    fprintf(stderr, "\nwrong number of byte received (%d)\n", n);
	  }
	  return 1;
	}
	else{
	  for(i=0; i<n; i++){
	    spt[i]=msg[i];
	  }
	}
      }
      timeout.tv_sec=0;
      timeout.tv_usec=SELECTTO;
      FD_ZERO(&rfds);
      FD_SET(fdser, &rfds);

      if(bl==1)
	break;      
      /* # get single frequency point end # */

      /* = decode single frequency point = */

      /* rearrange bytes in int32_t data type */
      for(i=0; i<6; i++){
	wave[i]=0x0;
	for(k=0; k<4; k++){
	  wave[i]|=((uint32_t)spt[i*4+k])<<(k*8);
	}      
      }
      freq_idx=spt[24]|(spt[25]<<8);
      if(freq_idx>=npts){
	fprintf(stderr,
		"selected number of point differs from LiteVNA setting\n");
	fprintf(stderr,
		"(WW) .s2p file incomplete\n");
	return 1;
      }
      
      /* get forward pwr from "channel 0 outgoing wave" */
      fwd_pwr=sqrt( ((double)wave[0]*(double)wave[0])+
		    ((double)wave[1]*(double)wave[1]) );
    

      /* get s11, s21 (cplx) */      
      s11_re[freq_idx]=(double)wave[2]/fwd_pwr;
      s11_im[freq_idx]=(double)wave[3]/fwd_pwr;
      s21_re[freq_idx]=(double)wave[4]/fwd_pwr;
      s21_im[freq_idx]=(double)wave[5]/fwd_pwr;

      /* # decode single frequency point end # */

    }

  }
  
  return 0;
}
