/*
 *   s2p half to full 
 * 
 *      (c) by Lapo Pieri 2022-2023
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Send bugs reports, comments, critique, etc, to
 *
 *        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
 */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include "s2ph2f.h"


int main(int argc, char **argv){
  int ml=1, clo;
  char linebuff[LINEBUFFLEN+1], *lb;
  double f, s1, s2, s3, s4;
  
  /* = cmd line scan = */
  while((clo=getopt(argc, argv, "hd"))!=-1){
    switch(clo){
    case 'h':
      use();
      return 0;
      break;
    case 'd':
      doc();
      return 0;
    default:
      return 1;
    }
  }
  /* # cmd line scan end # */
  
  /* = .s2p file parsing = */
  while(ml){
    /* read .n2s file from stdin */
    if(fgets(linebuff, LINEBUFFLEN, stdin)==NULL){
      ml=0;
      continue;
    }
    
    /* flush off spaces and tabs */
    lb=(char*)linebuff; skipspace(&lb);
    stripnewline(&lb);

    /* discard comment lines (beginning with '#') */
    if(lb[0]=='!')
      continue;
    if(lb[0]=='#')
      printf("%s\n", lb);
    else{
      scanf("%lf %lf %lf %lf %lf", &f, &s1, &s2, &s3, &s4);
      printf("%lf %lf  %lf  %lf  %lf  %lf  %lf  %lf  %lf\n",
	     f, s1, s2, s3, s4, s3, s4, s1, s2);
    }
  }
  /* # .n2s file parsing end # */

  return 0;
}


void use(void){

  printf("\ns2p half to full ("
	 ANSI_FG_GREEN ANSI_BOLD "s2ph2f" ANSI_FG_WHITE ANSI_NORMAL
	 ") usage:\n\n");
  printf(
     "s2ph2f [options] <.s2p half filled> > <.s2p full fille simmetrical>\n");
  printf("-h this help\n");
  printf("-d show documentation and theory used \
(use $ s2ph2f -d |less)\n\n");
}

void doc(void){
  
  printf("\n  s2p half to full ("
	 ANSI_FG_GREEN ANSI_BOLD "s2ph2f" ANSI_FG_WHITE ANSI_NORMAL
	 ") is a simple script that copy S21 to S12\n");
  printf("  and S11 to S22 for simmetrical 2-ports devices; usefull with\n");
  printf("  2-ports VNAs and simmetrical networks\n\n");

}

unsigned int skipspace(char **b){
  while(((*b)[0]==' ' || (*b)[0]=='\t') && (*b)[0]!='\0') 
    (*b)++;
  if(*b[0]=='\0')
    return 1;
  else
    return 0;
}

unsigned int skipnotspace(char **b){
  while(*b[0]!=' ' && *b[0]!='\t' && *b[0]!='\0')
    (*b)++;
  if(*b[0]=='\0')
    return 1;
  else
    return 0;
}

unsigned int stripnewline(char **b){
  while((*b)[strlen(*b)-1]=='\r' || (*b)[strlen(*b)-1]=='\n')
    (*b)[strlen(*b)-1]='\0';
  
  return 0;
}
